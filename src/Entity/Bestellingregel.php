<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BestellingregelRepository")
 */
class Bestellingregel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $aantal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Bestelling", inversedBy="bestellingregels")
     */
    private $bestelling;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Recept", inversedBy="bestellingregels")
     */
    private $recept;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAantal(): ?int
    {
        return $this->aantal;
    }

    public function setAantal(int $aantal): self
    {
        $this->aantal = $aantal;

        return $this;
    }

    public function getBestelling(): ?Bestelling
    {
        return $this->bestelling;
    }

    public function setBestelling(?Bestelling $bestelling): self
    {
        $this->bestelling = $bestelling;

        return $this;
    }

    public function getRecept(): ?Recept
    {
        return $this->recept;
    }

    public function setRecept(?Recept $recept): self
    {
        $this->recept = $recept;

        return $this;
    }
}
