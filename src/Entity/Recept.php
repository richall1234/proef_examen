<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReceptRepository")
 */
class Recept
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $naam;

    /**
     * @ORM\Column(type="integer")
     */
    private $prijs_per_liter;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bereidingswijze;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Bestellingregel", mappedBy="recept")
     */
    private $bestellingregels;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Fruit")
     */
    private $fruit;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Fruit", mappedBy="recept")
     */
    private $fruits;

    public function __construct()
    {
        $this->bestellingregels = new ArrayCollection();
        $this->fruit = new ArrayCollection();
        $this->fruits = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNaam(): ?string
    {
        return $this->naam;
    }

    public function setNaam(string $naam): self
    {
        $this->naam = $naam;

        return $this;
    }

    public function getPrijsPerLiter(): ?int
    {
        return $this->prijs_per_liter;
    }

    public function setPrijsPerLiter(int $prijs_per_liter): self
    {
        $this->prijs_per_liter = $prijs_per_liter;

        return $this;
    }

    public function getBereidingswijze(): ?string
    {
        return $this->bereidingswijze;
    }

    public function setBereidingswijze(string $bereidingswijze): self
    {
        $this->bereidingswijze = $bereidingswijze;

        return $this;
    }

    /**
     * @return Collection|Bestellingregel[]
     */
    public function getBestellingregels(): Collection
    {
        return $this->bestellingregels;
    }

    public function addBestellingregel(Bestellingregel $bestellingregel): self
    {
        if (!$this->bestellingregels->contains($bestellingregel)) {
            $this->bestellingregels[] = $bestellingregel;
            $bestellingregel->setRecept($this);
        }

        return $this;
    }

    public function removeBestellingregel(Bestellingregel $bestellingregel): self
    {
        if ($this->bestellingregels->contains($bestellingregel)) {
            $this->bestellingregels->removeElement($bestellingregel);
            // set the owning side to null (unless already changed)
            if ($bestellingregel->getRecept() === $this) {
                $bestellingregel->setRecept(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Fruit[]
     */
    public function getFruit(): Collection
    {
        return $this->fruit;
    }

    public function addFruit(Fruit $fruit): self
    {
        if (!$this->fruit->contains($fruit)) {
            $this->fruit[] = $fruit;
        }

        return $this;
    }

    public function removeFruit(Fruit $fruit): self
    {
        if ($this->fruit->contains($fruit)) {
            $this->fruit->removeElement($fruit);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->naam;
    }

    /**
     * @return Collection|Fruit[]
     */
    public function getFruits(): Collection
    {
        return $this->fruits;
    }


}
