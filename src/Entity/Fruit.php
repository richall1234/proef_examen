<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FruitRepository")
 */
class Fruit
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $naam;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $seizoen;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Recept", inversedBy="fruits")
     */
    private $recept;

    public function __construct()
    {
        $this->recept = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNaam(): ?string
    {
        return $this->naam;
    }

    public function setNaam(string $naam): self
    {
        $this->naam = $naam;

        return $this;
    }

    public function getSeizoen(): ?string
    {
        return $this->seizoen;
    }

    public function setSeizoen(string $seizoen): self
    {
        $this->seizoen = $seizoen;

        return $this;
    }

    public function __toString()
    {
        return $this->naam;
    }

    /**
     * @return Collection|Recept[]
     */
    public function getRecept(): Collection
    {
        return $this->recept;
    }

    public function addRecept(Recept $recept): self
    {
        if (!$this->recept->contains($recept)) {
            $this->recept[] = $recept;
        }

        return $this;
    }

    public function removeRecept(Recept $recept): self
    {
        if ($this->recept->contains($recept)) {
            $this->recept->removeElement($recept);
        }

        return $this;
    }
}
