<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200416180901 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE recept_fruit (recept_id INT NOT NULL, fruit_id INT NOT NULL, INDEX IDX_918562E6C6BF5295 (recept_id), INDEX IDX_918562E6BAC115F0 (fruit_id), PRIMARY KEY(recept_id, fruit_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE recept_fruit ADD CONSTRAINT FK_918562E6C6BF5295 FOREIGN KEY (recept_id) REFERENCES recept (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE recept_fruit ADD CONSTRAINT FK_918562E6BAC115F0 FOREIGN KEY (fruit_id) REFERENCES fruit (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE bestellingregel ADD recept_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE bestellingregel ADD CONSTRAINT FK_DA487844C6BF5295 FOREIGN KEY (recept_id) REFERENCES recept (id)');
        $this->addSql('CREATE INDEX IDX_DA487844C6BF5295 ON bestellingregel (recept_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE recept_fruit');
        $this->addSql('ALTER TABLE bestellingregel DROP FOREIGN KEY FK_DA487844C6BF5295');
        $this->addSql('DROP INDEX IDX_DA487844C6BF5295 ON bestellingregel');
        $this->addSql('ALTER TABLE bestellingregel DROP recept_id');
    }
}
